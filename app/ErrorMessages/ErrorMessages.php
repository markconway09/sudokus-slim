<?php
namespace ErrorMessages;

class ErrorMessages{
    public static function error($num){
        switch($num){
            case 0:
                return json_encode(array('Error'=>'Invalid token.'));
            break;
            case 1:
                return json_encode(array('Error'=>'Invalid header(s).'));
            break;
            case 2:
                return json_encode(array('Error'=>'Invalid number.'));
            break;
        }
    }
}