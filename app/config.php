<?php
require_once '../vendor/autoload.php';

define('APP_DIR', __DIR__);
define('BASE_DIR', realpath(__DIR__ . '/..'));
define('TEMPLATES_DIR', APP_DIR . '/templates');
define('DATA_DIR', APP_DIR . '/data');
define('SUDOKUS_DIR', DATA_DIR . '/sudokus');
define('PLAYER_DIR', DATA_DIR . '/player');
define('GAME_DIR', DATA_DIR . '/game');