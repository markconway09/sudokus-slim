<?php
namespace Sudoku\Controllers;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use \ErrorMessages\ErrorMessages as Error;
use \Sudoku\Model\SudokuDB as SDB;
use \Player\Model\PlayerDB as PDB;

class SudokuController {

  private $twig;

  public function __construct() {
    // Twig initialization
    $loader = new \Twig\Loader\FilesystemLoader(TEMPLATES_DIR);
    $this->twig = new \Twig\Environment($loader);
  }

  public function mGETaHOME(Request $request, Response $response, array $args){
    $html = $this->twig->render('home.twig');
    $response->getBody()->write($html);
    return $response;
  }

  public function mGETaDOCS(Request $request, Response $response, array $args){
    $html = $this->twig->render('doc.twig');
    $response->getBody()->write($html);
    return $response;
  }

  public function mGETaSUDOKUS(Request $request, Response $response, array $args) {
    if(!$player=PDB::checkToken($request)){
      $response->getBody()->write(Error::error(0));
      return $response;
    }
    $sudokus=SDB::loadSudokuFromJSON(SUDOKUS_DIR."/sudokus.json",$player["id"]);
    $response->getBody()->write($sudokus);
    return $response;
  }

  public function mGETaSUDOKU(Request $request, Response $response, array $args){
    if(!$player=PDB::checkToken($request)){
      $response->getBody()->write(Error::error(0));
      return $response;
    }
    if(!isset($args["num"])){
      $response->getBody()->write(Error::error(2));
      return $response;
    }
    $sudokus=SDB::loadSudokuFromJSON(SUDOKUS_DIR."/sudokus.json",$player["id"],$args["num"]);
    $response->getBody()->write($sudokus);
    return $response;
  }

  public function mPUTaSUDOKU(Request $request, Response $response, array $args){
    if(!$player=PDB::checkToken($request)){
      $response->getBody()->write(Error::error(0));
      return $response;
    }
    if(!isset($args["num"])){
      $response->getBody()->write(Error::error(2));
      return $response;
    }
    $num = $args["num"];
    $sud = $request->hasHeader('sudoku')?$request->getHeader('sudoku')[0]:null;
    $time = $request->hasHeader('time')?intval($request->getHeader('time')[0]):null;
    
    $sudoku=SDB::saveSudokuState(GAME_DIR."/games.json",$player["id"],$num,$sud,$time);

    $response->getBody()->write($sudoku);
    return $response;
  }

  public function mPUTaRESET(Request $request, Response $response, array $args){
    if(!$player=PDB::checkToken($request)){
      $response->getBody()->write(Error::error(0));
      return $response;
    }
    if(isset($args["num"])){
      SDB::resetSudoku($player["id"],$args["num"]);
      $response->getBody()->write("Reset sudoku nº".$args["num"]);
      return $response;
    }
    SDB::resetSudoku($player["id"]);
    $response->getBody()->write("Reset all sudokus");
    return $response;
  }

};