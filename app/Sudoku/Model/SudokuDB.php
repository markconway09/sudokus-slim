<?php
namespace Sudoku\Model;

use \Player\Model\PlayerDB as PDB;

class SudokuDB{
    public static function buildSudoku($id, $level, $description, $sudoku, $answer) {
        return [ 
          'id' => $id,
          'level' => $level, 
          'description' => $description, 
          'sudoku' => $sudoku, 
          'answer' => $answer
        ];
    }
    public static function buildGame($sudoku, $time, $state){
        return [
            'sudoku' => $sudoku,
            'time' => $time,
            'state' => $state
        ];
    }
    public static function buildThreeLinesArray($sudoku) {
        if ($sudoku === false) return false;
        return [
          '% ' . $sudoku['level'] . ' ' . $sudoku['description'],
          $sudoku['sudoku'],
          $sudoku['answer'],
        ];
    }
    public static function loadSudokuFromFolder($folder, $id) {
        // Load the description file
        $txtFilePath = $folder . '/sud' . $id . '.txt';
        if (!file_exists($txtFilePath)) return false;
        $txt = file_get_contents($txtFilePath);
        if ($txt === false) return false;
        $txtLines = explode("\n", $txt);
        if (count($txtLines) < 3) return false;
        list($level, $description, $description2) = $txtLines;
      
        // Load the sudoku file
        $sudFilePath = $folder . '/sud' . $id . '.sud';
        if (!file_exists($sudFilePath)) return false;
        $sud = file_get_contents($sudFilePath);
        if ($sud === false) return false;
        $sudLines = explode("\n", $sud);
        $sudoku = implode("", $sudLines);
        
        // Load the answer file
        $ansFilePath = $folder . '/sud' . $id . '.ans';
        if (!file_exists($ansFilePath)) return false;
        $ans = file_get_contents($ansFilePath);
        if ($ans === false) return false;
        $ansLines = explode("\n", $ans);
        $answer = implode("", $ansLines);

        return self::buildSudoku($id, $level, $description, $sudoku, $answer);
    }
    public static function convertFolderToTXT($folder, $file, $from, $to){
        $lines = [];
        for($id = $from; $id <= $to; $id++) {
            $sudoku = self::loadSudokuFromFolder($folder, $id);
            if ($sudoku === false) continue;
            $sudokuLines = self::buildThreeLinesArray($sudoku);
            $lines = array_merge($lines, $sudokuLines);
        }
        $txt = implode("\n", $lines) . "\n";
        return file_put_contents($file, $txt);
    }
    public static function convertFolderToJSON($folder, $file, $from, $to){
        $sudokus = [];
        for($id = $from; $id <= $to; $id++) {
            $sudoku = self::loadSudokuFromFolder($folder, $id);
            if ($sudoku === false) continue;
            $sudokus[$id] = $sudoku;
        }
        
        $json = json_encode($sudokus,JSON_PRETTY_PRINT);
        return file_put_contents($file, $json);
    }
    public static function convertFolderToCSV($folder, $file, $from, $to){
        $lines = [];
        for($id = $from; $id <= $to; $id++) {
            $sudoku = self::loadSudokuFromFolder($folder, $id);
            if ($sudoku === false) continue;
            $cols = [ 
            $sudoku['level'], 
            '"' . addcslashes($sudoku['description'], '"') . '"', 
            '"' . addcslashes($sudoku['sudoku'], '"') . '"', 
            '"' . addcslashes($sudoku['answer'], '"') . '"', 
            ];
            $lines[] = implode(":", $cols);
        }
        $csv = implode("\n", $lines) . "\n";
        return file_put_contents($file, $csv);
    }
    public static function loadSudokuFromTXT($file, $id){
        // Load the file
        if (!file_exists($file)) return false;
        $txt = file_get_contents($file);
        if ($txt === false) return false;
        $txtLines = explode("\n", $txt);
        $lineNumber = ($id - 1) * 3;
        if (count($txtLines) < $lineNumber + 3) return false;
        $line1 = $txtLines[$lineNumber];
        list($ignore, $level, $description) = explode(' ', $line1);
        $sudoku = $txtLines[$lineNumber + 1];
        $answer = $txtLines[$lineNumber + 2];
        return self::buildSudoku($id, $level, $description, $sudoku, $answer);
    }
    public static function loadSudokuFromJSON($file, $id=null, $num=null){
        // Load the file
        
        $games=self::loadSudokuFromGames();

        if (!file_exists($file)) return false;
        $json = file_get_contents($file);
        if ($json === false) return false;
        $sudokus = json_decode($json, true);
        if ($sudokus === false) return false;
        if($num!=null){
            if(isset($games[$id][$num])){
                return json_encode(array($sudokus[$num],$games[$id][$num]));
            }else{
                return json_encode($sudokus[$num]);
            }
        }else{
            return json_encode($sudokus);
        }
    }
    public static function loadSudokuFromCSV($file, $id){
        // Load the file
        if (!file_exists($file)) return false;
        $csv = file_get_contents($file);
        if ($csv === false) return false;
        $lines = explode("\n", $csv);
        if (count($lines) < $id) return false;
        $line = $lines[$id - 1];
        $parts = str_getcsv($line, ':', '"', '\\');
        if (count($parts) != 4) return false;
        list($level, $description, $sudoku, $answer) = $parts;
        $sudoku = self::buildSudoku($id, $level, $description, $sudoku, $answer);
        return $sudoku;
    }

    public static function loadSudokuFromGames(){
        return json_decode(file_get_contents(GAME_DIR."/games.json"),true);
    }

    public static function saveSudokuState($file, $player_id, $sudoku_id, $sud, $time){
        $sudokusGame=self::loadSudokuFromGames();
        $sudokus=json_decode(self::loadSudokuFromJSON(SUDOKUS_DIR."/sudokus.json"),true);
        if(!isset($sudokusGame[$player_id][$sudoku_id])){
            $sudokusGame[$player_id][$sudoku_id]=self::buildGame("",0,0);
        }
        if($sud!=null){
            $sudokusGame[$player_id][$sudoku_id]["sudoku"]=$sud;
            if($sud==$sudokus[$sudoku_id]["sudoku"]){
                $sudokusGame[$player_id][$sudoku_id]["state"]=0;
            }else if($sud==$sudokus[$sudoku_id]["answer"]){
                $sudokusGame[$player_id][$sudoku_id]["state"]=2;
            }else{
                $sudokusGame[$player_id][$sudoku_id]["state"]=1;
            }
        }
        if($time!=null) $sudokusGame[$player_id][$sudoku_id]["time"]=$time;

        $players = json_decode(PDB::loadPlayerAdmin(),true);
        $players[$player_id]["finished"]=0;
        foreach($sudokusGame[$player_id] as $sud){
            if($sud["state"]==2){
                $players[$player_id]["finished"]++;
            }
        }
        file_put_contents(PLAYER_DIR."/players.json",json_encode($players,JSON_PRETTY_PRINT));

        $json = json_encode($sudokusGame,JSON_PRETTY_PRINT);
        file_put_contents($file, $json);
        return json_encode($sudokusGame[$player_id][$sudoku_id]);
    }

    public static function resetSudoku($player, $sud=null){
        $sudokus = self::loadSudokuFromGames();
        if($sud!=null){
            if(!$sudokus[$player][$sud]) return false;
            unset($sudokus[$player][$sud]);
            file_put_contents(GAME_DIR."/games.json",json_encode($sudokus,JSON_PRETTY_PRINT));
        }else{
            foreach($sudokus[$player] as $sudoku){
                unset($sudoku);
            }
            file_put_contents(GAME_DIR."/games.json",json_encode($sudokus,JSON_PRETTY_PRINT));
        }
    }
}