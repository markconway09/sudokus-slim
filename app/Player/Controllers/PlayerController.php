<?php
namespace Player\Controllers;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use \ErrorMessages\ErrorMessages as Error;
use \Player\Model\PlayerDB as PDB;
use \Sudoku\Model\SudokuDB as SDB;

class PlayerController {

    public function mPOSTaREGISTER(Request $request, Response $response, array $args){
        if(!$request->hasHeader('email')){
            $response->getBody()->write(Error::error(1));
            return $response;
        }
        $email=$request->getHeader('email')[0];
        $player=PDB::addPlayer("",$email);
        if(!SDB::convertFolderToJSON(DATA_DIR."/suds",SUDOKUS_DIR."/sudokus.json",1,100)) return $response;
        $response->getBody()->write(json_encode(array("token"=>$player["token"])));
        return $response;
    }

    public function mGETaRANKING(Request $request, Response $response, array $args){
        if(!PDB::checkToken($request)){
            $response->getBody()->write(Error::error(0));
            return $response;
        }
        $players=PDB::loadPlayer();
        $array=json_decode($players, true);
        $players=PDB::array_sort($array,"finished");
        $response->getBody()->write(json_encode($players,JSON_PRETTY_PRINT));
        return $response;
    }

    public function mPUTaPLAYER(Request $request, Response $response, array $args){
        if(!$player=PDB::checkToken($request)){
            $response->getBody()->write(Error::error(0));
            return $response;
        }
        if(!$request->hasHeader('name') && !$request->hasHeader('email')){
            $response->getBody()->write(Error::error(1));
            return $response;
        }
        $players=json_decode(PDB::loadPlayer(),true);
        if($request->hasHeader('name')){
            $players[$player["id"]]["name"] = $request->getHeader('name')[0];
        }
        if($request->hasHeader('email')){
            $players[$player["id"]]["email"] = $request->getHeader('email')[0];
        }
        
        if(!PDB::updatePlayers($players)) return false;
        $response->getBody()->write(json_encode($players[$player["id"]]));
        return $response;
    }

    public function mGETaPLAYERS(Request $request, Response $response, array $args){
        if(!PDB::checkToken($request)){
            $response->getBody()->write(Error::error(0));
            return $response;
        }
        $players=PDB::loadPlayer();
        $response->getBody()->write($players);
        return $response;
    }

    public function mGETaPLAYER(Request $request, Response $response, array $args){
        if(!$p=PDB::checkToken($request)){
            $response->getBody()->write(Error::error(0));
            return $response;
        }
        $players=json_decode(PDB::loadPlayer(),true);
        $player = isset($args["num"])?$players[$args["num"]]:$players[$p["id"]];
        $response->getBody()->write(json_encode($player));
        return $response;
    }
}