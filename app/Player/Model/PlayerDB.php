<?php

namespace Player\Model;

class PlayerDB
{
  public static function buildPlayer($id, $name, $email, $token)
  {
    return [
      'id' => $id,
      'name' => $name,
      'email' => $email,
      'token' => $token,
      'finished' => 0
    ];
  }
  public static function checkToken($request)
  {
    if (!$request->hasHeader('token')) return false;
    $token = $request->getHeader('token')[0];
    $players = json_decode(self::loadPlayerAdmin(), true);
    foreach ($players as $player) {
      if ($player["token"] == $token) return $player;
    }
    return false;
  }
  public static function addPlayer($name, $email)
  {
    $file = PLAYER_DIR . "/players.json";
    if (!file_exists($file)) return false;
    $json = file_get_contents($file);
    if ($json === false) return false;
    $players = json_decode($json, true);
    $token = bin2hex(random_bytes(16));
    $player = self::buildPlayer(is_array($players) ? count($players) : 0, $name, $email, $token);
    $players[] = $player;
    file_put_contents($file, json_encode($players,JSON_PRETTY_PRINT));
    return $player;
  }

  public static function updatePlayers($players)
  {
    $file = PLAYER_DIR . "/players.json";
    if (!file_exists($file)) return false;
    return file_put_contents($file, json_encode($players,JSON_PRETTY_PRINT));
  }

  public static function loadPlayer($id = null)
  {
    $file = PLAYER_DIR . "/players.json";
    if (!file_exists($file)) return false;
    $json = file_get_contents($file);
    if ($json === false) return false;
    $array = json_decode($json,true);
    $aux = [];
    foreach($array as $a){
      $aux[]=[
        "id" => $a["id"],
        "name" => $a["name"],
        "email" => $a["email"],
        "finished" => $a["finished"]
      ];
    }
    if (!is_null($id)) {
      // WITH ID
      if ($aux === false) return false;
      if (!isset($aux[$id])) return false;
      $sudoku = $aux[$id];
      return json_encode($sudoku);
    } else {
      // NO ID
      return json_encode($aux);
    }
  }

  public static function loadPlayerAdmin($id = null)
  {
    $file = PLAYER_DIR . "/players.json";
    if (!file_exists($file)) return false;
    $json = file_get_contents($file);

    if (!is_null($id)) {
      // WITH ID
      if ($json === false) return false;
      $players = json_decode($json, true);
      if ($players === false) return false;
      if (!isset($players[$id])) return false;
      $sudoku = $players[$id];
      return json_encode($sudoku);
    } else {
      // NO ID
      if ($json === false) return false;
      return $json;
    }
  }

  public static function array_sort($array, $on, $order=SORT_DESC)
  {
      $new_array = array();
      $sortable_array = array();

      if (count($array) > 0) {
          foreach ($array as $k => $v) {
              if (is_array($v)) {
                  foreach ($v as $k2 => $v2) {
                      if ($k2 == $on) {
                          $sortable_array[$k] = $v2;
                      }
                  }
              } else {
                  $sortable_array[$k] = $v;
              }
          }

          switch ($order) {
              case SORT_ASC:
                  asort($sortable_array);
              break;
              case SORT_DESC:
                  arsort($sortable_array);
              break;
          }

          foreach ($sortable_array as $k => $v) {
              $new_array[$k] = $array[$k];
          }
      }

      return $new_array;
  }
}
