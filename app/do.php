<?php
use Slim\Factory\AppFactory;

require_once 'config.php';

$app = AppFactory::create();

$app->addErrorMiddleware(1, 0, 0);

// GET
$app->get('/','\Sudoku\Controllers\SudokuController:mGETaHOME');
$app->get('/docs','\Sudoku\Controllers\SudokuController:mGETaDOCS'); // DOCS
$app->get('/api/player','\Player\Controllers\PlayerController:mGETaPLAYER'); // PLAYER INFO
$app->get('/api/player/all','\Player\Controllers\PlayerController:mGETaPLAYERS'); // PLAYER LIST
$app->get('/api/player/{num}','\Player\Controllers\PlayerController:mGETaPLAYER'); // OTHER PLAYER INFO
$app->get('/api/ranking','\Player\Controllers\PlayerController:mGETaRANKING'); // RANKING
$app->get('/api/sudoku','\Sudoku\Controllers\SudokuController:mGETaSUDOKUS'); // SUDOKUS
$app->get('/api/sudoku/{num}','\Sudoku\Controllers\SudokuController:mGETaSUDOKU'); // SUDOKU

// POST
$app->post('/api/register','\Player\Controllers\PlayerController:mPOSTaREGISTER'); // REGISTER

// PUT
$app->put('/api/player','\Player\Controllers\PlayerController:mPUTaPLAYER'); // PUT PLAYER INFO
$app->put('/api/sudoku/{num}','\Sudoku\Controllers\SudokuController:mPUTaSUDOKU'); // SAVE SUDOKU
$app->put('/api/reset','\Sudoku\Controllers\SudokuController:mPUTaRESET'); // RESET ALL SUDOKUS
$app->put('/api/reset/{num}','\Sudoku\Controllers\SudokuController:mPUTaRESET'); // RESET ONE SUDOKU

$app->run();
